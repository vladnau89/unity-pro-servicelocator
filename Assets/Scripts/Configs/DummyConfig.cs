﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "DummyConfig", menuName = "Configs/Dummy", order = 0)]
    public class DummyConfig : ScriptableObject
    {
        
    }
}