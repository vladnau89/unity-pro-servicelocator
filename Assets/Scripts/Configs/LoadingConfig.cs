﻿using System;
using System.Collections.Generic;
using Tasks;
using UnityEditor;
using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "LoadingConfig", menuName = "Configs/Loading Config", order = 0)]
    public class LoadingConfig : ScriptableObject
    {
        [SerializeField] private List<MonoScript> _tasks = new List<MonoScript>();
        
        public List<ILoadingTask> GetTaskList()
        {
            List<ILoadingTask> results = new List<ILoadingTask>();
            foreach (var task in _tasks)
            {
                var instance = Activator.CreateInstance(task.GetClass());
                results.Add((ILoadingTask)instance);
            }

            return results;
        }

    }
}