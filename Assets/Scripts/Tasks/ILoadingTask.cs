﻿using System.Collections;

namespace Tasks
{
    public interface ILoadingTask
    {
        IEnumerator Do();
    }
}