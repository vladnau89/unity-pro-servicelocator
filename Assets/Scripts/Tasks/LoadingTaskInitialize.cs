﻿using System.Collections;
using Services;

namespace Tasks
{
    public sealed class LoadingTaskInitialize : ILoadingTask
    {
        public IEnumerator Do()
        {
            var initializables = ServiceLocator.GetServices<IInitializable>();
            foreach (IInitializable initializable in initializables)
            {
                initializable.Init();
            }

            yield return null;
        }
    }
}