﻿namespace Tasks
{
    public interface IInitializable
    {
        void Init();
    }
}