﻿using System;
using Configs;
using Services;
using UnityEngine;

namespace DefaultNamespace
{
    public class Launcher : MonoBehaviour
    {
        private IApplicationManager m_ApplicationManager;

        private void Awake()
        {
            m_ApplicationManager = ServiceLocator.GetService<IApplicationManager>();
            m_ApplicationManager.StartEvent += ApplicationManagerOnStartEvent;
        }

        private void ApplicationManagerOnStartEvent()
        {
            StartCoroutine(ServiceLocator.GetService<ILoadingManager>().Load());
        }
    }
}