﻿using System;
using UnityEngine;

namespace Configs
{
    public class ApplicationManager : MonoBehaviour, IApplicationManager
    {
        public event Action StartEvent;
        
        private void Start()
        {
            StartEvent?.Invoke();
        }
    }
}