﻿using System;

namespace Configs
{
    public interface IApplicationManager
    {
        event Action StartEvent;
    }
}