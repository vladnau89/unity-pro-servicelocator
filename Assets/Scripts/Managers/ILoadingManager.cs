﻿using System.Collections;

namespace Configs
{
    public interface ILoadingManager
    {
        IEnumerator Load();
    }
}