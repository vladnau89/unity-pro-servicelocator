﻿using System;
using System.Collections;
using Tasks;
using UnityEngine;

namespace Configs
{
    public class LoadingManager : MonoBehaviour, ILoadingManager
    {
        [SerializeField] private LoadingConfig loadingConfig;


        public IEnumerator Load()
        {
            foreach (ILoadingTask task in loadingConfig.GetTaskList())
            {
                yield return task.Do();
            }
        }
    }
}