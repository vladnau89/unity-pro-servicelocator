﻿using System;

namespace Configs
{
    public interface IConfigManager
    {
        void Load<T>(string path, Action<T> callback) where T :  UnityEngine.Object;
    }
}