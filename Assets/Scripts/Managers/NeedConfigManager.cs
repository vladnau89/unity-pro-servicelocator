﻿using Services;
using Tasks;
using UnityEngine;

namespace Configs
{
    public class NeedConfigManager : MonoBehaviour, IInitializable
    {
        [SerializeField] private string configPath;

        public void Init()
        {
            ServiceLocator.GetService<IConfigManager>().Load<DummyConfig>(configPath, onConfigLoaded);
        }

        private void onConfigLoaded(DummyConfig config)
        {
            Debug.LogWarning($"{config.name} is loaded!");
        }
    }
}