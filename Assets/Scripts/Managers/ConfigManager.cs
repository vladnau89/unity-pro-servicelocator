﻿using System;
using Tasks;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Configs
{
    public class ConfigManager : MonoBehaviour, IConfigManager
    {
        public void Load<T>(string path, Action<T> callback) where T : Object
        {
            ResourceRequest resourceRequest = Resources.LoadAsync<T>(path);
            resourceRequest.completed += operation =>
            {
                callback?.Invoke((T)resourceRequest.asset);
            };
        }
    }
}